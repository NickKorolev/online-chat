const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const PATHS = {
    src: path.join(__dirname, '../src'),
    dist: path.join(__dirname, '../dist'),
    assets: 'assets/'
}

module.exports = {

    externals: {
        paths: PATHS
    },
    entry: {
        app: `${PATHS.src}/index.js`,
    },
    output: {
        filename:`${PATHS.assets}js/[name].js`,
        path: PATHS.dist,
        publicPath: "/"
    },
    module:{
        rules:[
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader',
                options: {
                    name: 'assets/fonts/[name].[ext]'
                }
            },
            {
                test: /test\.js$/,
                use: 'mocha-loader',
                exclude: /node_modules/,
            }, {
                test:/\.(png|jpg|gif|svg)$/,
                loader: 'url-loader',
                options: {
                    name: 'img/[name].[ext]',
                    limit: 50000
                }
            },{
                test:/\.js$/,
                loader: 'babel-loader',
                exclude: '/node_modules/',
            },{
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    ],

            },{
                test: /\.css$/,
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },  {
                        loader: 'postcss-loader',
                        options: { sourceMap: true , config: {path: `${PATHS.src}/assets/js/postcss.config.js`} }
                    },
                ],

            }]
    },
    devServer: {
        overlay: true
    },
    plugins: [
        // new MiniCssExtractPlugin({
        //     filename: `${PATHS.assets}css/[name].css`,
        // }),
        new HtmlWebpackPlugin({
            hash: false,
            template: `${PATHS.src}/index.html`,
            filename: "./index.html",
            inject: false,
        }),

        new CopyWebpackPlugin([
            {from: `${PATHS.src}/assets/img` ,to: `${PATHS.assets}img`},
            {from: `${PATHS.src}/assets/fonts` ,to: `${PATHS.assets}fonts`},
            {from: `${PATHS.src}/assets/static` ,to: `${PATHS.assets}static`},
        ]),

    ],
    watch: true,

}
