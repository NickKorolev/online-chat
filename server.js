const express = require('express');
const WebSocket = require('ws');
const path = require('path')
const http = require('http');
const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server( {server} );
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const MongoClient = require("mongodb").MongoClient;
// const objectId = require("mongodb").ObjectID;
const MONGO = new MongoClient('mongodb://b5cc0fe3cfe8bb0606b03ccb6f262747:HelloWorld@9a.mongo.evennode.com:27017/b5cc0fe3cfe8bb0606b03ccb6f262747',
    {useNewUrlParser: true , useUnifiedTopology: true});

const dbMongo = {
    mongoPassword: "HelloWorld",
    dbName: "b5cc0fe3cfe8bb0606b03ccb6f262747",
    putMessage(value) {
        MONGO.connect((err , client) => {
            const db = client.db(this.dbName);
            const collection = db.collection("msg");
            collection.insertOne(value , function (err, result) {
                if(err){
                    return console.log(err)
                }
            })
        })
},
    getMessages(req , res) {
        MONGO.connect((err, client) => {
            const db = client.db(this.dbName);
            const collection = db.collection("msg");
            const msg = collection.find().toArray((err, results) => {
                res.send(JSON.stringify({msg: results}))
            });
        })
    },
    putUsers(value) {
        MONGO.connect((err , client) => {
            const db = client.db(this.dbName);
            const collection = db.collection("users");
            collection.insertOne(value , function (err, result) {
                if(err){
                    return console.log(err)
                }
            })
        })
    },
    getUsers(req , res) {
        MONGO.connect((err, client) => {
            const db = client.db(this.dbName);
            const collection = db.collection("users");
            const users = collection.find().toArray((err, results) => {
                res.send(JSON.stringify({users: results}))
            });
        })
    }
};

app.post('/sendMsg', jsonParser, (req, res) => {
    try {
        dbMongo.putMessage(req.body);
        res.send(true);
    } catch (e) {
        res.send(e)
    }

});

app.get('/all-msg' , function (req , res) {
    try{
        dbMongo.getMessages(req, res)
    } catch(e) {
        res.send(e)
    }

});

app.get('/getUsers' , function (req , res) {
    try{
         dbMongo.getUsers(req, res)
     } catch (e) {

     }
});
app.put('/putUser', jsonParser, function (req, res) {
    try{
        const result = req.body;
        dbMongo.putUsers(result)
        res.send(true)
        onlineObs.subscribe(result);
        for(let i = 0 ; i < allObs.observers.length; i++){
            if(result.deviceID === allObs.observers[i].deviceID){
               // console.log("We have some device")
                return
            } else{

            }
        }

        allObs.subscribe(result)
    } catch (e) {
        res.send(e)
    }

})

wss.on('connection' ,ws => {
    ws.on('message', data => {
        wss.clients.forEach(function each(client) {
            if (client.readyState === WebSocket.OPEN) {
                client.send(data);
            }
        });
    });
});

app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname,'./assets')));

// это для отслеживание количества участников
class Observer {
    constructor(){
        this.observers = [];
    }

    subscribe(fn) {
        this.observers.push(fn)
    }

    unsubscribe(fn) {
        this.observers = this.observers.filter(
            subscriber => subscriber.deviceID !== fn.deviceID
        );
    }
    broadcast(data){
        this.observers.forEach( function (subscriber){
            return subscriber(data)
        });
    }
}

const onlineObs = new Observer();
const allObs = new Observer();

// для удаления онлайн участника
app.put('/delUser', jsonParser, function (req, res) {
    try{
        const result = req.body
        onlineObs.unsubscribe(result);

        res.send(true);
    } catch (e) {
        res.send(e)
    }

})
// смена имени посмотреть как это сделать??
app.put('/changeNick', jsonParser, (req, res) => {
    try{
        res.send(true);
        allObs.observers.find(item => {
            if(item.deviceID === req.body.deviceID) {
                item.nickName = req.body.nickName;

            }
            if(item.nickName === req.body.nickName &&
                item.deviceID !== req.body.deviceID){
                for(let i = 1; i < allObs.observers.length; i++){
                    item.nickName = `${req.body.nickName}_${[i]}`
                }
            }

        }) 
    }catch (e) {
        res.send(e)
    }
   
});

//открытие нашего сайта
app.get('/', function (req, res) {
    onlineObs.unsubscribe();
    res.send(path.join(__dirname, 'dist' , 'index.html'));
});

app.get('/getIp', function (req, res) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    res.send(JSON.stringify({ip : ip}))
});

// все участники чата количество
app.get('/getAllUsers', function (req, res) {
    let allUsers = allObs.observers.length;
    res.send(JSON.stringify({allUsers : allUsers}))
});

// все участники чата онлайн количество
app.get('/getUsersOnline' , function (req , res) {
    let onlineUsers = onlineObs.observers.length
    res.send(JSON.stringify({onlineUsers : onlineUsers}))

});

// запуск сервера на порте
const PORT = process.env.PORT || 3001;
server.listen(PORT , '0.0.0.0',  function(err) {
    console.log('server runninng at port...' + PORT );
});










