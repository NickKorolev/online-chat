import {addListeners} from "./controller";
import {initControll} from "./controller";
import {modalListeners} from "./modals/controller";

document.addEventListener("DOMContentLoaded", init);

function init ()  {
    initControll();
    modalListeners();
};