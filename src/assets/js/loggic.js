import {model} from "./model";
import {Person} from "./model";
import {printMessage} from "./messages/loggic";
import {printUsers} from "./messages/loggic";
import {modalModel} from "./modals/model";

export const setUsersOnline = (users) => {
    model.xhr.open('GET', '/getUsersOnline', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            users.setOnlineUsers(JSON.parse(model.xhr.response).onlineUsers);
        }
    };
};
export const setAllUsers = (users) => {
    model.xhr.open('GET', '/getAllUsers', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            users.setAllUsers(JSON.parse(model.xhr.response).allUsers);
        }
    };
};
export const getUsersOnline = (users) => {
    model.onlineUsers.innerHTML = users.getOnlineUsers();
};

export const getAllUsers = (users) => {
    model.allUsers.innerHTML = users.getAllUsers();
};
export const setIP = (person) => {
    model.xhr.open('GET', '/getIp', true)
    model.xhr.send();
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            person.setIp(JSON.parse(model.xhr.response).ip);
        }
    };
};

export const getIP = (person) => {
    model.myName.innerHTML = person.getIp() ;
    modalModel.modalIp.innerHTML = person.getIp();
};

// export const sendMsg = (text, nickName, mobileNickName, IP, date , deviceID) => {
//     model.xhr.open('POST', '/sendMsg', true)
//     model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
//     model.xhr.send(JSON.stringify({
//         msg: text,
//         nickName: nickName,
//         mobileNickName: mobileNickName,
//         IP: IP,
//         date: date,
//         deviceID : deviceID,
//     }))
// };

/*export const sendSidebar = (nickName, mobileNickName , IP , deviceID) => {
    model.xhr.open('GET', '/getUsers', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName: mobileNickName,
        IP: IP,
        deviceID: deviceID,
    }))
};*/

export const delUser = (nickName ,mobileNickName, IP ,deviceID  ) => {
    model.xhr.open('PUT', '/delUser', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName:mobileNickName,
        IP: IP,
        deviceID: deviceID ,
    }))

};

export const putUser = (nickName ,mobileNickName, IP , deviceID ) => {
    model.xhr.open('PUT', '/putUser', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName:mobileNickName,
        IP: IP,
        deviceID: deviceID ,
    }))
};

export const getUsers = () => {
    model.xhr.open('GET', '/getUsers', true);
    model.xhr.send();
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const result = JSON.parse(model.xhr.response);
            console.log(JSON.parse(model.xhr.response) + '12312312')
            printUsers(result.users)
        }
    };
};

export const getMsg = () => {
    model.xhr.open('GET', '/all-msg', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const result = JSON.parse(model.xhr.response);
            result.msg.forEach(function (item) {
                printMessage(item)
            })
        }
    };
};

