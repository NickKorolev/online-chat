import {model} from "./model";
import {Person} from "./model";
import {printMessage} from "./messages/logic";
import {printUsers} from "./messages/logic";


export const setUsersOnline = (users) => {
    model.xhr.open('GET', '/getUsersOnline', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            users.setOnlineUsers(JSON.parse(model.xhr.response).onlineUsers);
        }
    };
};
export const getUsersOnline = (users) => {
    model.onlineUsers.innerHTML = users.getOnlineUsers();
};

export const setIP = (person) => {
    model.xhr.open('GET', '/getIp', true)
    model.xhr.send();
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            person.setIp(JSON.parse(model.xhr.response).ip);
        }
    };
};



export const getIP = (person) => {
    model.myName.innerHTML = person.getIp();
};

export const sendMsg = (text, nickName, mobileNickName, IP, date , deviceID) => {
    model.xhr.open('PUT', '/sendMsg', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        msg: text,
        nickName: nickName,
        mobileNickName: mobileNickName,
        IP: IP,
        deviceID : deviceID,
        date: date,
    }))
};

export const sendSidebar = (nickName, mobileNickName , IP) => {
    model.xhr.open('GET', '/getUsers', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName: mobileNickName,
        IP: IP,
    }))
};

export const delUser = (nickName ,mobileNickName, IP ,deviceID  ) => {
    model.xhr.open('PUT', '/delUser', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName:mobileNickName,
        IP: IP,
        deviceID: deviceID ,
    }))

};

export const putUser = (nickName ,mobileNickName, IP , deviceID ) => {
    model.xhr.open('PUT', '/putUser', true)
    model.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    model.xhr.send(JSON.stringify({
        nickName: nickName,
        mobileNickName:mobileNickName,
        IP: IP,
        deviceID: deviceID ,
    }))
};

export const getMsg = () => {
    model.xhr.open('GET', '/all-msg', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(JSON.parse(model.xhr.response))
            const result = JSON.parse(model.xhr.response);
            result.forEach(function (item) {
                console.log(item)
                printMessage(item)
            })
        }
    };
};
export const getUsers = () => {
    model.xhr.open('GET', '/getUsers', true);
    model.xhr.send()
    model.xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const result = JSON.parse(model.xhr.response);
            printUsers(result)
        }
    };
};