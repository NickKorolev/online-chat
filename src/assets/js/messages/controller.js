import {printMessagesModel} from "./model";
import {putUser, sendMsg} from "../loggic";
import {delUser} from "../loggic";
import {msgToDb, printMessage, setStatus} from "./loggic";
import {Person} from "../model";
import {getUsers} from "../loggic";


export const initPrintMessagesControll = (person) => {

    printMessagesModel.ws.onopen = () => {
        if(!localStorage.getItem('deviceID')){
            localStorage.setItem('deviceID', Math.floor(Math.random() * 100000));
        }
        person.setDeviceID(localStorage.getItem('deviceID'))
        setStatus('status--online logo-user__status');
        putUser(person.getNick() , person.getMobileNickName() , person.getIp() , localStorage.getItem('deviceID'))
        getUsers();
    };

    window.onbeforeunload = () => {
        delUser(person.getNick() , person.getMobileNickName() , person.getIp() , localStorage.getItem('deviceID'))
    };

    printMessagesModel.ws.onerror =  () => {
        console.log('error')
    };

    printMessagesModel.ws.onclose = () => {
        setStatus('status--offline logo-user__status');
        delUser(person.getNick() , person.getMobileNickName() , person.getIp() , localStorage.getItem('deviceID'))
    };

    printMessagesModel.ws.onmessage = response => {
        getUsers();
        //printMessage(JSON.parse(response.data), person);
    };

    printMessagesModel.form.addEventListener('submit', event => {
        event.preventDefault();
        printMessagesModel.ws.send(JSON.stringify({
            msg: printMessagesModel.input.value,
            nickName: person.getNick() ? person.getNick() : null,
            mobileNickName: person.getMobileNickName() ? person.getMobileNickName() : null,
            IP: person.getIp(),
            date:printMessagesModel.getDate(),
            deviceID: localStorage.getItem('deviceID'),
        }));
        const data = {
            msg: printMessagesModel.input.value,
            nickName: person.getNick() ? person.getNick() : null,
            mobileNickName: person.getMobileNickName() ? person.getMobileNickName() : null,
            IP: person.getIp(),
            date:printMessagesModel.getDate(),
            deviceID: localStorage.getItem('deviceID')
        }
        msgToDb(data);
        printMessage(data)
        //printMessage(printMessagesModel.input.value, person.getNick() ,person.getMobileNickName() ,person.getIp() , printMessagesModel.getDate(), localStorage.getItem('deviceID'))
        printMessagesModel.input.value = '';
    });

};
