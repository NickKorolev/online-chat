import {printMessagesModel} from "./model";
import {sendMsg} from "../loggic";
import {sendSidebar} from "../loggic";
import {model, Person} from "../model";
import {modalListeners} from "../modals/controller";

export const setStatus = (value) => {
    printMessagesModel.status.className = value;
};

export const msgToDb = (value) => {
    printMessagesModel.xhr.open('POST', '/sendMsg', true);
    printMessagesModel.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    printMessagesModel.xhr.send(JSON.stringify(value));
};

export const  printMessage = (value, person) => {

    const div = document.createElement('div');
    if (localStorage.getItem('deviceID') === value.deviceID) {
        div.className = "history-box__msg msg msg--personal";
    } else {
        div.className = "history-box__msg msg msg--users";
    }
    const time = document.createElement('div');
    time.className = 'msg__time';
    time.innerHTML = value.date ? value.date : printMessagesModel.getDate();

    const nickName = document.createElement('div');
    nickName.className = 'msg__name';
    nickName.innerHTML = value.nickName ? value.nickName : value.IP;

    const txt = document.createElement('div');
    txt.className = 'msg__txt';
    txt.innerHTML = value.msg;

    div.append(nickName);
    div.append(txt);
    div.append(time);

    printMessagesModel.messages.append(div);

};

export const  printUsers = (...args) =>{
    let users = [...args];
    console.log(users);
    users = users[0];
    console.log(users)
    printMessagesModel.setUserSideBar();
    printMessagesModel.usersSidebar.parentNode.removeChild(printMessagesModel.usersSidebar);

    const sideBar = document.createElement('div');
    sideBar.className = 'users-sidebar';
    users.forEach(item => {
        const div = document.createElement('div');
        div.className = "users-sidebar__profile profile-sidebar";

        const logo = document.createElement('div');
        logo.className = 'profile-sidebar__logo logo-user';


        const logoImg = document.createElement('div');
        logoImg.className = 'logo-user__img';
        logoImg.innerHTML = 'U';

        const name = document.createElement('div');
        name.className = 'users-sidebar__name';
        name.innerHTML = item.nickName ? item.nickName : item.IP;

        const status = document.createElement('div');
        status.className = 'logo-user__status status--online';

        logo.append(logoImg)
        div.append(logo);
        logo.append(status);
        div.append(name);
        sideBar.append(div)
    })

    printMessagesModel.listUsers.append(sideBar);

    //if(person) sendSidebar(logo , person.getNick() , status)
}
