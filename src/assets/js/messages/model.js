export const printMessagesModel = {
    xhr: new XMLHttpRequest(),
    status : document.getElementById('status'),
    input : document.getElementById('input'),
    form : document.querySelector('.form-send'),
    messages : document.querySelector('.history-box'),
    usersSidebar : document.querySelector('.users-sidebar'),
    ws : new WebSocket('ws://smart-chat.eu-4.evennode.com'),
    date : new Date(),
    div: document.querySelector('.users-sidebar__profile.profile-sidebar'),
    listUsers: document.querySelector('.sidebar-list__users'),
    setUserSideBar() {
        this.usersSidebar = document.querySelector('.users-sidebar');
    },
    getDate() {
        const hour = this.date.getHours() < 10 ? '0' + this.date.getHours() : this.date.getHours();
        const minutes = this.date.getMinutes() < 10 ? '0' + this.date.getMinutes() : this.date.getMinutes();
        return `${hour}:${minutes}`
    }
};