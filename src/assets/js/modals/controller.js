import {modalModel} from "./model"
import {Person} from "../model";
import {closeModal} from "./loggic";
import {changeNickName} from "./loggic";

export const modalListeners = (person) => {
    modalModel.settingsBtn.addEventListener('click' , function () {
        modalModel.overlay.classList.add('active')
        modalModel.modal.classList.add('active')
    })
    modalModel.closeModal.addEventListener('click' , closeModal)
    modalModel.overlay.addEventListener('click' , closeModal)
    modalModel.installNick.addEventListener('click' , function (e) {
        e.preventDefault();
        person.setNick(modalModel.installInput.value);
        closeModal()
        changeNickName(modalModel.installInput.value);
    })
};









