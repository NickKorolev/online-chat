import {modalModel} from "./model";
import {model} from "../model";

export const closeModal = () => {
    modalModel.overlay.classList.remove('active')
    modalModel.modal.classList.remove('active')
}

export const changeNickName = (value) =>{
    modalModel.xhr.open('PUT', '/changeNick', true)
    modalModel.xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    modalModel.xhr.send(JSON.stringify({
        nickName: value,
        deviceID: localStorage.getItem('deviceID')
    }))
};