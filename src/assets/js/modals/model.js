export const modalModel = {
    settingsBtn : document.querySelector('.settings-chat'),
    overlay : document.querySelector('.overlay'),
    closeModal : document.querySelector('.modal-settings__close'),
    installNick : document.querySelector('.install-name__submit'),
    modal : document.querySelector('.modal-settings'),
    installInput : document.querySelector('.install-name__input'),
    modalIp : document.querySelector('.modal-settings__ip'),
    xhr: new XMLHttpRequest(),
}