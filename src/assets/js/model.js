export const model = {
    myName : document.getElementById('myName'),
    xhr: new XMLHttpRequest(),
    onlineUsers: document.querySelector('.online-info__number'),
    allUsers: document.querySelector('.all-info__number'),

};

export class Person {
    constructor() {
        this.IP = null;
        this.nickName = null;
        this.deviceID = null;
        this.mobileNickName = null;
    }
    setIp(value){
        this.IP = value
    };
    getIp(){
        return this.IP
    }
    setNick(value){
        this.nickName = value
    };
    getNick(){
        return this.nickName
    };
    setDeviceID(value){
        this.deviceID = value
    }
    getDeviceID(){
        return this.deviceID
    }
    setMobileNickName(value){
        this.mobileNickName = value
    }
    getMobileNickName(){
        return this.mobileNickName
    }

}
export class Users {
    constructor() {
        this.allUsers = null;
        this.onlineUsers = null;
    }
    setAllUsers(value){
        this.allUsers = value
    };
    getAllUsers(){
        return this.allUsers
    };
    setOnlineUsers(value){
        this.onlineUsers = value
    };
    getOnlineUsers(){
        return this.onlineUsers
    };
}



